﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PK.Container
{
    public class Container : IContainer
    {
        private Dictionary<Type, Type> implement_1;
        private Dictionary<Type, object> implement_2;
        private Dictionary<Type, Func<object>> implement_3;

        public Container()
        {
            this.implement_1 = new Dictionary<Type, Type>();
            this.implement_2 = new Dictionary<Type, object>();
            this.implement_3 = new Dictionary<Type, Func<object>>();
        }

        public void Register(System.Reflection.Assembly assembly)
        {
            foreach (var t in assembly.GetTypes())
            {
                if (t.IsClass)
                {
                    foreach (var i in t.GetType().GetInterfaces())
                    {
                        this.implement_1[t] = i.GetType();
                    }
                }
            }
        }

        public void Register(Type type)
        {
            foreach (var i in type.GetInterfaces())
            {
                this.implement_1[i.GetType()] = type;
            }
        }

        public void Register<T>(T impl) where T : class
        {
            this.implement_2[typeof(T).GetType()] = impl;
        }

        public void Register<T>(Func<T> provider) where T : class
        {
            this.implement_3[typeof(T).GetType()] = provider;
        }

        public T Resolve<T>() where T : class
        {
            var r1 = this.implement_1[typeof(T).GetType()];
            if (r1 != null)
            {
                var instance = (T)Activator.CreateInstance(r1);
                return instance as T;
            }
            var r2 = this.implement_2[typeof(T).GetType()];
            if (r2 != null)
            {
                return r2 as T;
            }
            var r3 = this.implement_3[typeof(T).GetType()];
            if (r3 != null)
            {
                return r3() as T;
            }

            throw new PK.Container.UnresolvedDependenciesException("Nie można spełnić zależności");
        }

        public object Resolve(Type type)
        {
            // OK- Działa
            System.Type r = type;
            var instance = (object)Activator.CreateInstance(this.implement_1[r.GetType()]);

            if (instance == null)
            {
                throw new PK.Container.UnresolvedDependenciesException("Nie można spełnić zależności");
            }
            else
            {
                return instance as object;
            }
        }
    }
}
