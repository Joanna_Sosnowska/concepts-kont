﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Container.NET_TEST
{
    interface I1
    {
        void M1();
        void M2();
    }

    class C1 : I1
    {

        public void M1()
        {
            Console.WriteLine("M1 z C1");
        }

        public void M2()
        {
            Console.WriteLine("M2 z C1");
        }
    }
   
    class Program
    {
        static void Main(string[] args)
        {
            PK.Container.IContainer container = new PK.Container.Container();
            container.Register(typeof(C1));
            var i = container.Resolve<I1>();
            i.M1();
            var j = container.Resolve(typeof(I1));
            i.M2();

            Console.ReadKey();
        }
    }
}
